#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QFileSystemModel>
#include <QDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QFileDialog>
#include <QDesktopServices>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    QString copy_dir;
    QString copy_name;
    ~MainWindow();

private slots:
    void on_treeView_clicked(const QModelIndex &index);

    void on_deleteButton_clicked();
    void rename_file();
    void copy_file();
    void paste_file();
    void open_file();

    void on_addFolderButton_clicked();

    void on_addFileButton_clicked();

    void on_listView_doubleClicked(const QModelIndex &index);

    void on_showHiddenButton_clicked();

private:
    Ui::MainWindow *ui;
    QFileSystemModel *dirModel;
    QFileSystemModel *fileModel;

};
#endif // MAINWINDOW_H
