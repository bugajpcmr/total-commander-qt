#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

        // Dir
        QString mPath = "C:/";
        dirModel = new QFileSystemModel(this);
        // filters
        dirModel->setFilter(QDir::NoDotAndDotDot |
                            QDir::AllDirs);
        dirModel->setRootPath(mPath);
        // Add dir to view
        ui->treeView->setModel(dirModel);

        // FILES
        fileModel = new QFileSystemModel(this);
        // filters
        fileModel->setFilter(QDir::NoDotAndDotDot |
                            QDir::AllDirs | QDir::Files);
        fileModel->setRootPath(mPath);

        // Add files to view
        ui->listView->setModel(fileModel);

        ui->listView->setContextMenuPolicy(Qt::CustomContextMenu);
          connect(ui->listView, &QListView::customContextMenuRequested, [this](QPoint pos){
                  //QModelIndex index = ui->listView->currentIndex();
                  QMenu *menu = new QMenu(this);
                  menu->addAction(tr("Open"), this,
                                  SLOT(open_file()));
                  menu->addAction(tr("Delete"), this,
                                  SLOT(on_deleteButton_clicked()));
                  menu->addAction(tr("Rename"), this,
                                  SLOT(rename_file()));
                  menu->addAction(tr("Copy"), this,
                                  SLOT(copy_file()));
                  menu->addAction(tr("Paste"), this,
                                  SLOT(paste_file()));
                  menu->popup(ui->listView->viewport()->mapToGlobal(pos));
            });

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_treeView_clicked(const QModelIndex &index)
{
    // Get the full path of the item that's user clicked on
    QString mPath = dirModel->fileInfo(index).absoluteFilePath();
    ui->listView->setRootIndex(fileModel->setRootPath(mPath));
}

void MainWindow::on_deleteButton_clicked()
{
    QMessageBox::StandardButton reply;
     reply = QMessageBox::question(this, "Delete", "Are you sure you want to remove this file?",
                                   QMessageBox::Yes|QMessageBox::No);
     if (reply == QMessageBox::Yes) {
         QModelIndex index = ui->listView->currentIndex();
         fileModel->remove(index);
     } else {
       return;
     }
}
void MainWindow::rename_file()
{
    QModelIndex index = ui->listView->currentIndex();
        QFileSystemModel *model = (QFileSystemModel*)ui->listView->model();
        QString path = model->filePath(index);
        QString name = model->fileName(index);
        QString dir = path;
        dir.remove(dir.size() - name.size(), name.size());
        QString mName = name;
                mName = QInputDialog::getText(this, tr("Rename"), tr("New Name"));
        if(model->isDir(index))
        {
            QDir currentDir(path);
            currentDir.rename(path, dir+mName);
        }
        else
        {
            QFile file(path);
            if(file.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                //Interact with the file
                file.close();
                if(file.rename(path, dir+mName))
                        qDebug() << "Renamed";
            }
        }

}
void MainWindow::copy_file()
{
    QModelIndex index = ui->listView->currentIndex();
    QFileSystemModel *model = (QFileSystemModel*)ui->listView->model();
    QString path = model->filePath(index);
    QString name = model->fileName(index);
    QString dir = path;
    dir.remove(dir.size() - name.size(), name.size());

    copy_dir = dir;
    copy_name = name;
}
void MainWindow::paste_file()
{
    QModelIndex index = ui->treeView->currentIndex();
    QFile::copy(copy_dir+copy_name, dirModel->filePath(index)+"/copy_"+copy_name);
}

void MainWindow::on_addFolderButton_clicked()
{
    QModelIndex index = ui->treeView->currentIndex();
    QString mName = QInputDialog::getText(this, tr("Create Folder"), tr("Folder Name"));
            if(!mName.isEmpty()){
                dirModel->mkdir(index, mName);
            }
}

void MainWindow::on_addFileButton_clicked()
{
    QModelIndex index = ui->treeView->currentIndex();
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), dirModel->filePath(index), tr("PHP file (*.php, *php3)"));

           if(!fileName.isEmpty()){
               QFile sFile(fileName);
               if(!sFile.open(QFile::WriteOnly | QFile::Text)){
                   qDebug() << "we have an error =( " << " " << sFile.errorString();
               }else{
                   QTextStream out(&sFile);
                   out << "";

                   sFile.flush();
                   sFile.close();
               }
           }
}

void MainWindow::on_listView_doubleClicked(const QModelIndex &index)
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(fileModel->filePath(index)));
}

void MainWindow::open_file()
{
    QModelIndex index = ui->listView->currentIndex();
    QDesktopServices::openUrl(QUrl::fromLocalFile(fileModel->filePath(index)));
}

void MainWindow::on_showHiddenButton_clicked()
{
    if(ui->showHiddenButton->text() == "Show Hidden")
    {
        fileModel->setFilter(QDir::Hidden | QDir::NoDotAndDotDot |
                             QDir::AllDirs | QDir::Files);
        ui->showHiddenButton->setText("Hide Hidden");
    }else
    {
        ui->showHiddenButton->setText("Show Hidden");
        fileModel->setFilter(QDir::NoDotAndDotDot |
                             QDir::AllDirs | QDir::Files);
    }

}
